<?php

use Illuminate\Database\Seeder;
use App\Curso;

class cursoseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Curso::create([
        	'curso'=>'Agronomia',
        	'semestre'=>'10'
        ]);

        Curso::create([
			'curso'=>'Farmacia',
			'semestre'=>'10'        	
        ]);

        Curso::create([
        	'curso'=>'Sistemas de Informacao',
        	'semestre'=>'8'
        ]);

        Curso::create([
        	'curso'=>'Direito',
        	'semestre'=>'10'
        ]);

        Curso::create([
        	'curso'=>'Educacao Fisica',
        	'semestre'=>'10'
        ]);
    }
}
