<?php

use Illuminate\Database\Seeder;
use App\Aluno;

class alunoseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Aluno::create([
    		'nome'=>'Joao',
    		'ra'=>'001122',
    		'id_curso'=>1
    	]);

    	Aluno::create([
    		'nome'=>'Maria',
    		'ra'=>'112233',
    		'id_curso'=>2
    	]);

    	Aluno::create([
    		'nome'=>'Eva',
    		'ra'=>'332211',
    		'id_curso'=>3
    	]);

    	Aluno::create([
    		'nome'=>'Marcus',
    		'ra'=>'221100',
    		'id_curso'=>4
    	]);

    	Aluno::create([
    		'nome'=>'Jose',
    		'ra'=>'774411',
    		'id_curso'=>5
    	]);
    }
}
