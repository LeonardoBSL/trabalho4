<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return voidl
     */
    public function run()
    {
        $this->call(cursoseed::class);
        $this->call(alunoseed::class);
    }
}
