<?php
 
namespace App\Http\Controllers;

use App\Curso;
use DB;
use Illuminate\Http\Request;
use Mockery\Exception;
use Validator;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
            private $atributos = ['curso','semestre'];

    public function index(Request $request)
    {
        $qtd = $request->input('qtd');

        try{
            return response()->json( [Curso::paginate($qtd)], 200 );
        }catch( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            $validacao = $this->validar($request);

            //$validacao->after(function ($validacao) {
                //$validacao->errors()->add('campo1', 'Verifique se os campos forão preenchidos coretamente!');
    //            $validacao->errors()->add('campo2', 'Mensagem de validacao2!');
    //            $validacao->errors()->add('campo3', 'Mensagem de validacao2!');
            //});

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }

            $curso = new Curso();

            $curso->fill( $request->all() );
            $curso->save();

            if( $curso ){
                return response()->json( [$curso], 201 );
            }else{
                return response()->json( ["mensagem" => "Erro ao cadastrar curso"], 400 );
            }

            return $curso;
        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $curso = DB::select('select * from cursos where id = ? ', [$id]);


        return $curso;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $curso = DB::update('update cursos set curso = ?, semestre=? where id = ?',[$request->input('curso'),$request->input('ra'),$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //return "Excluir...";
        $curso = Curso::find( $id );
        $curso->delete();
        return $curso;
    }


    public function validar( $request ){

        $validator = Validator::make($request->only( $this->atributos ),[
            'curso' => 'required|min:3|max:30',
            'semestre'=>'required|min:1|max:2',
        ]);

        return $validator;
    }
}
