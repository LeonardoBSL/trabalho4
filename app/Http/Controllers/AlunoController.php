<?php
 
namespace App\Http\Controllers;

use DB;
use App\Aluno;
use Illuminate\Http\Request;
use Mockery\Exception;
use Validator;

class AlunoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

        private $atributos = ['nome','ra', 'id_curso'];

    public function index(Request $request)
    {
        $qtd = $request->input('qtd');


        try{
            return response()->json( [Aluno::paginate($qtd)], 200 );
        }catch( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            $validacao = $this->validar($request);

            //$validacao->after(function ($validacao) {
                //$validacao->errors()->add('Erro de preenchimento', 'Verifique se os campos forão preenchidos coretamente!');
    //            $validacao->errors()->add('campo2', 'Mensagem de validacao2!');
    //            $validacao->errors()->add('campo3', 'Mensagem de validacao2!');
            //});

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }

            $aluno = new Aluno();

            $aluno->fill( $request->all() );
            $aluno->save();

            if( $aluno ){
                return response()->json( [$aluno], 201 );
            }else{
                return response()->json( ["mensagem" => "Erro ao cadastrar aluno"], 400 );
            }

            return $aluno;
        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aluno = DB::select('select * from alunos where id = ? ', [$id]);


        return $aluno;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aluno = DB::update('update alunos set nome = ?, ra=?, id_curso = ? where id = ?',[$request->input('nome'),$request->input('ra'),$request->input('id_curso'),$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //return "Excluir...";
        $aluno = Aluno::find( $id );
        $aluno->delete();
        return $aluno;
    }


    public function validar( $request ){

        $validator = Validator::make($request->only( $this->atributos ),[
            'nome' => 'required|min:3|max:30',
            'ra'=>'required|min:6|max:6',
            'id_curso' => 'required|numeric'

        ]);

        return $validator;
    }
}
