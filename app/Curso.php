<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $fillable=['curso','semestre'];

    public function Aluno(){
    	return $this->hasMany('App\Aluno','id_curso','id');

    }
}
