<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $fillable= ['nome','ra','id_curso'];
    
    public function Curso(){
    	return $this->belongsTo('App\Curso','id_curso','id');

    }
}
